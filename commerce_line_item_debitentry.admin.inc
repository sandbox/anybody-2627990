<?php

/**
 * @file
 * White label entity type editing UI.
 */

/**
 * UI controller.
 */
class CommerceLineItemDebitentryTypeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = commerce_line_item_debitentry_get_string('admin menu description');
    return $items;
  }
}

/**
 * Generates the commerce_line_item_debitentry type editing form.
 */
function commerce_line_item_debitentry_type_form($form, &$form_state, Entity $commerce_line_item_debitentry_type, $op = 'edit') {

  if ($op == 'clone') {
    $commerce_line_item_debitentry_type->label .= ' (cloned)';
    $commerce_line_item_debitentry_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $commerce_line_item_debitentry_type->label,
    '#description' => t('The human-readable name of this @commerce_line_item_debitentry-type.', array('@commerce_line_item_debitentry-type' => commerce_line_item_debitentry_get_string('type label'))),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable type name.
  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($commerce_line_item_debitentry_type->name) ? $commerce_line_item_debitentry_type->name : '',
    '#maxlength' => 32,
    '#disabled' => $commerce_line_item_debitentry_type->hasStatus(ENTITY_IN_CODE) && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'commerce_line_item_debitentry_type_load',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this @commerce_line_item_debitentry-type. It must only contain lowercase letters, numbers, and underscores.', array('@commerce_line_item_debitentry-type' => commerce_line_item_debitentry_get_string('type label'))),
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => $commerce_line_item_debitentry_type->weight,
    '#description' => t('When showing @commerce_line_item_debitentrys, those with lighter (smaller) weights get listed before @commerce_line_item_debitentrys with heavier (larger) weights.', array('@commerce_line_item_debitentrys' => commerce_line_item_debitentry_get_string('entity plural label'))),
    '#weight' => 10,
  );

  $form['data']['#tree'] = TRUE;
  $form['data']['supports_revisions'] = array('#type' => 'checkbox',
    '#title' => t('Support revisions'),
    '#default_value' => !empty($commerce_line_item_debitentry_type->data['supports_revisions']),
    '#description' => t('Enable revision support for this @commerce_line_item_debitentry-type.', array('@commerce_line_item_debitentry-type' => commerce_line_item_debitentry_get_string('type label'))),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save @commerce_line_item_debitentry-type', array('@commerce_line_item_debitentry-type' => commerce_line_item_debitentry_get_string('type label'))),
    '#weight' => 40,
  );
  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete @commerce_line_item_debitentry-type', array('@commerce_line_item_debitentry-type' => commerce_line_item_debitentry_get_string('type label'))),
    '#weight' => 45,
    '#limit_validation_errors' => array(),
    '#submit' => array('commerce_line_item_debitentry_type_form_submit_delete'),
    '#access' => !$commerce_line_item_debitentry_type->hasStatus(ENTITY_IN_CODE) && $op != 'add' && $op != 'clone'
  );
  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function commerce_line_item_debitentry_type_form_submit(&$form, &$form_state) {
  $commerce_line_item_debitentry_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  $commerce_line_item_debitentry_type->save();
  $form_state['redirect'] = commerce_line_item_debitentry_get_string('admin menu path');
}

/**
 * Form API submit callback for the delete button.
 */
function commerce_line_item_debitentry_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = commerce_line_item_debitentry_get_string('admin menu path') . '/' . $form_state['commerce_line_item_debitentry_type']->name . '/delete';
}
