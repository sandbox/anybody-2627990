<?php

/**
 * @file
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
* Act on commerce_line_item_debitentry label entities being loaded from the database.
*
* This hook is invoked during white label entity loading, which is handled by
* entity_load(), via the EntityCRUDController.
*
* @param $entities
*   An array of commerce_line_item_debitentry entities being loaded, keyed by id.
*
* @see hook_entity_load()
*/
function hook_commerce_line_item_debitentry_load($entities) {
  $result = db_query('SELECT pid, foo FROM {mytable} WHERE pid IN(:ids)', array(':ids' => array_keys($entities)));
  foreach ($result as $record) {
    $entities[$record->pid]->foo = $record->foo;
  }
}

/**
* Respond when a white label entity is inserted.
*
* This hook is invoked after the white label entity is inserted into the database.
*
* @param $commerce_line_item_debitentry
*   The white label entity that is being inserted.
*
* @see hook_entity_insert()
*/
function hook_commerce_line_item_debitentry_insert($commerce_line_item_debitentry) {
  db_insert('mytable')
    ->fields(array(
      'pid' => $commerce_line_item_debitentry->pid,
      'extra' => $commerce_line_item_debitentry->extra,
    ))
    ->execute();
}

/**
* Act on a white label entity being inserted or updated.
*
* This hook is invoked before the white label entity is saved to the database.
*
* @param $commerce_line_item_debitentry
*   The white label entity that is being inserted or updated.
*
* @see hook_entity_presave()
*/
function hook_commerce_line_item_debitentry_presave($commerce_line_item_debitentry) {
  $commerce_line_item_debitentry->extra = 'foo';
}

/**
* Respond to a white label entity being updated.
*
* This hook is invoked after the white label entity has been updated in the database.
*
* @param $commerce_line_item_debitentry
*   The $commerce_line_item_debitentry that is being updated.
*
* @see hook_entity_update()
*/
function hook_commerce_line_item_debitentry_update($commerce_line_item_debitentry) {
  db_update('mytable')
    ->fields(array('extra' => $commerce_line_item_debitentry->extra))
    ->condition('pid', $commerce_line_item_debitentry->pid)
    ->execute();
}

/**
* Respond to white label entity deletion.
*
* This hook is invoked after the white label entity has been removed from the database.
*
* @param $commerce_line_item_debitentry
*   The white label entity that is being deleted.
*
* @see hook_entity_delete()
*/
function hook_commerce_line_item_debitentry_delete($commerce_line_item_debitentry) {
  db_delete('mytable')
    ->condition('pid', $commerce_line_item_debitentry->pid)
    ->execute();
}

/**
* Act on a white label entity that is being assembled before rendering.
*
* @param $commerce_line_item_debitentry
*   The white label entity entity.
* @param $view_mode
*   The view mode the white label entity is rendered in.
* @param $langcode
*   The language code used for rendering.
*
* The module may add elements to $commerce_line_item_debitentry->content prior to rendering. The
* structure of $commerce_line_item_debitentry->content is a renderable array as expected by
* drupal_render().
*
* @see hook_entity_prepare_view()
* @see hook_entity_view()
*/
function hook_commerce_line_item_debitentry_view($commerce_line_item_debitentry, $view_mode, $langcode) {
  $commerce_line_item_debitentry->content['my_additional_field'] = array(
    '#markup' => $additional_field,
    '#weight' => 10,
    '#theme' => 'mymodule_my_additional_field',
  );
}

/**
* Alter the results of entity_view() for commerce_line_item_debitentry label entities.
*
* @param $build
*   A renderable array representing the white label entity content.
*
* This hook is called after the content has been assembled in a structured
* array and may be used for doing processing which requires that the complete
* white label entity content structure has been built.
*
* If the module wishes to act on the rendered HTML of the white label entity rather than
* the structured content array, it may use this hook to add a #post_render
* callback. Alternatively, it could also implement hook_preprocess_commerce_line_item_debitentry().
* See drupal_render() and theme() documentation respectively for details.
*
* @see hook_entity_view_alter()
*/
function hook_commerce_line_item_debitentry_view_alter($build) {
  if ($build['#view_mode'] == 'full' && isset($build['an_additional_field'])) {
    // Change its weight.
    $build['an_additional_field']['#weight'] = -10;

    // Add a #post_render callback to act on the rendered HTML of the entity.
    $build['#post_render'][] = 'my_module_post_render';
  }
}

/**
 * Act on white label entity type being loaded from the database.
 *
 * This hook is invoked during white label entity type loading, which is handled by
 * entity_load(), via the EntityCRUDController.
 *
 * @param $types
 *   An array of commerce_line_item_debitentry label entities being loaded, keyed by white label entity type names.
 */
function hook_commerce_line_item_debitentry_type_load($types) {
  if (isset($types['main'])) {
    $types['main']->userCategory = FALSE;
    $types['main']->userView = FALSE;
  }
}

/**
 * Respond when a white label entity type is inserted.
 *
 * This hook is invoked after the white label entity type is inserted into the database.
 *
 * @param $type
 *   The white label entity type that is being inserted.
 */
function hook_commerce_line_item_debitentry_type_insert($type) {
  db_insert('mytable')
    ->fields(array(
      'id' => $type->id,
      'extra' => $type->extra,
    ))
    ->execute();
}

/**
 * Act on a white label entity type being inserted or updated.
 *
 * This hook is invoked before the white label entity type is saved to the
 * database.
 *
 * @param $type
 *   The white label entity type that is being inserted or updated.
 */
function hook_commerce_line_item_debitentry_type_presave($type) {
  $type->extra = 'foo';
}

/**
 * Respond to updates to a white label entity.
 *
 * This hook is invoked after the white label entity type has been updated in
 * the database.
 *
 * @param $type
 *   The white label entity type that is being updated.
 */
function hook_commerce_line_item_debitentry_type_update($type) {
  db_update('mytable')
    ->fields(array('extra' => $type->extra))
    ->condition('id', $type->id)
    ->execute();
}

/**
 * Respond to white label entity type deletion.
 *
 * This hook is invoked after the white label entity type has been removed from
 * the database.
 *
 * @param $type
 *   The white label entity type that is being deleted.
 */
function hook_commerce_line_item_debitentry_type_delete($type) {
  db_delete('mytable')
    ->condition('id', $type->id)
    ->execute();
}

/**
 * Define default white label entity type configurations.
 *
 * @return
 *   An array of default white label entity types, keyed by white label entity
 *   type names.
 */
function hook_default_commerce_line_item_debitentry_type() {
  $types['main'] = new commerce_line_item_debitentry_type(array(
      'type' => 'main',
      'label' => t('white label entity'),
      'weight' => 0,
      'locked' => TRUE,
  ));
  return $types;
}

/**
* Alter default white label entity type configurations.
*
* @param $defaults
*   An array of default white label entity types, keyed by type names.
*
* @see hook_default_commerce_line_item_debitentry_type()
*/
function hook_default_commerce_line_item_debitentry_type_alter(&$defaults) {
  $defaults['main']->label = 'custom label';
}

/**
 * Alter commerce_line_item_debitentry forms.
 *
 * Modules may alter the commerce_line_item_debitentry entity form regardless to which form it is
 * attached by making use of this hook or the white label entity type specifiy
 * hook_form_commerce_line_item_debitentry_edit_white label entity_TYPE_form_alter(). #entity_builders
 * may be used in order to copy the values of added form elements to the entity,
 * just as described by entity_form_submit_build_entity().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 *
 * @see commerce_line_item_debitentry_attach_form()
 */
function hook_form_commerce_line_item_debitentry_form_alter(&$form, &$form_state) {
  // Your alterations.
}

/**
 * Control access to commerce_line_item_debitentry label entities.
 *
 * Modules may implement this hook if they want to have a say in whether or not
 * a given user has access to perform a given operation on a white label entity.
 *
 * @param $op
 *   The operation being performed. One of 'view', 'edit' (being the same as
 *   'create' or 'update') and 'delete'.
 * @param $commerce_line_item_debitentry
 *   (optional) A white label entity to check access for. If nothing is given,
 *   access for all commerce_line_item_debitentry label entities is determined.
 * @param $account
 *   (optional) The user to check for. If no account is passed, access is
 *   determined for the global user.
 * @return boolean
 *   Return TRUE to grant access, FALSE to explicitly deny access. Return NULL
 *   or nothing to not affect the operation.
 *   Access is granted as soon as a module grants access and no one denies
 *   access. Thus if no module explicitly grants access, access will be denied.
 *
 * @see commerce_line_item_debitentry_access()
 */
function hook_commerce_line_item_debitentry_access($op, $commerce_line_item_debitentry = NULL, $account = NULL) {
  if (isset($commerce_line_item_debitentry)) {
    // Explicitly deny access for a 'secret' white label entity type.
    if ($commerce_line_item_debitentry->type == 'secret' && !user_access('custom permission')) {
      return FALSE;
    }
    // For commerce_line_item_debitentry label entities other than the default white label entity grant access.
    if ($commerce_line_item_debitentry->type != 'main' && user_access('custom permission')) {
      return TRUE;
    }
    // In other cases do not alter access.
  }
}

/**
 * @}
 */
