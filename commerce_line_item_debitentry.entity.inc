<?php

/**
 * @file
 * Holds entity related classes.
 */

/**
 * The commerce_line_item_debitentry entity controller.
 */
class CommerceLineItemDebitentryController extends EntityAPIController {

  /**
   * Overridden to add in more default values.
   */
  public function create(array $values = array()) {
    $values += array(
      'created' => REQUEST_TIME,
      'langcode' => LANGUAGE_NONE,
      'uid' => $GLOBALS['user']->uid,
    );
    return parent::create($values);
  }

}

/**
 * A commerce_line_item_debitentry entity.
 */
class CommerceLineItemDebitentryEntity extends Entity {

  public $created;
  public $changed;
  public $id;
  public $uid;
  public $type;
  public $label = '';
  public $status = 1;

  public function __construct($values = array()) {
    // Support creation with the type object also.
    if (isset($values['type']) && is_object($values['type'])) {
      $values['type'] = $values['type']->name;
    }
    parent::__construct($values, 'commerce_line_item_debitentry');
  }

  /**
   * Overridden to care about created and changed times.
   */
  public function save() {
    // Do not automatically set a created values for already existing entities.
    if (empty($this->created) && (!empty($this->is_new) || !$this->id)) {
      $this->created = REQUEST_TIME;
    }

    $commerce_line_item_debitentry_type = $this->type();
    if ($commerce_line_item_debitentry_type->supportsRevisions()) {
      $this->is_new_revision = isset($this->options) && !empty($this->options['create_revision']);
    }
    $this->changed = REQUEST_TIME;

    parent::save();
  }

  /**
   * Gets the associated commerce_line_item_debitentry type object.
   *
   * @return CommerceLineItemDebitentryType
   */
  public function type() {
    return commerce_line_item_debitentry_type_load($this->type);
  }

  /**
   * Implements a custom default URI
   */
  public function defaultUri() {
    return array(
      'path' => commerce_line_item_debitentry_get_string('base path') . '/' . $this->id,
    );
  }

  /**
   * Sets a new user.
   *
   * @param $account
   *   The user account object or the user account id (uid).
   */
  public function setUser($account) {
    $this->uid = is_object($account) ? $account->uid : $account;
  }

  /**
   * Gets the user account.
   *
   * @return
   *   The user account object.
   */
  public function user() {
    return user_load($this->uid);
  }

}

/**
 * A commerce_line_item_debitentry type entity.
 */
class CommerceLineItemDebitentryType extends Entity {

  public $label = '';
  public $weight = 0;

  /**
   * Helper method to check if the commerce_line_item_debitentry type supports revisions.
   *
   * @return bool
   *   TRUE if revision support is enabled, FALSE if not.
   */
  public function supportsRevisions() {
    return !empty($this->data['supports_revisions']);
  }

}
