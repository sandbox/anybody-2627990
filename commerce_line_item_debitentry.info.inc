<?php

/**
 * @file
 * Provides Entity metadata integration.
 */

/**
 * Extend the defaults.
 */
class CommerceLineItemDebitentryMetadataController extends EntityDefaultMetadataController {

  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $properties = &$info[$this->type]['properties'];

    $properties['type'] = array(
      'type' => 'commerce_line_item_debitentry',
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer commerce_line_item_debitentries',
      'required' => TRUE,
      'description' => commerce_line_item_debitentry_get_string('type label'),
    ) + $properties['type'];

//    $properties['label'] = array(
//      'type' => 'text',
//      'setter callback' => 'entity_property_verbatim_set',
//      'required' => TRUE,
//      'label' => t('Label'),
//      'schema field' => 'label',
//    );

    $properties['user'] = array(
      'label' => t("User"),
      'type' => 'user',
      'description' => t("The creator of the @commerce_line_item_debitentry.", array('@commerce_line_item_debitentry' => commerce_line_item_debitentry_get_string('entity label'))),
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_setter_method',
      'setter permission' => 'administer commerce_line_item_debitentries',
      'required' => TRUE,
      'schema field' => 'uid',
    );

    $properties['created'] = array(
      'label' => t("Date created"),
      'type' => 'date',
      'description' => t("The date the @commerce_line_item_debitentry was created.", array('@commerce_line_item_debitentry' => commerce_line_item_debitentry_get_string('entity label'))),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer commerce_line_item_debitentries',
      'schema field' => 'created',
    );

    $properties['changed'] = array(
      'label' => t("Date changed"),
      'type' => 'date',
      'schema field' => 'changed',
      'description' => t("The date the @commerce_line_item_debitentry was most recently updated.", array('@commerce_line_item_debitentry' => commerce_line_item_debitentry_get_string('entity label'))),
    );

    $properties['status'] = array(
      'label' => t("Status"),
      'type' => 'boolean',
      'schema field' => 'status',
      'description' => t("Whether the @commerce_line_item_debitentry is active (true) or not (false).", array('@commerce_line_item_debitentry' => commerce_line_item_debitentry_get_string('entity label'))),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer commerce_line_item_debitentries',
    );

    return $info;
  }
}
