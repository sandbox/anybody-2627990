<?php

/**
 * @file
 * Holds inline entity form related classes.
 */

/**
 * The inline form controller.
 */
class CommerceLineItemDebitentryEntityInlineEntityFormController extends EntityInlineEntityFormController {

  public function entityForm($entity_form, &$form_state) {
    $info = entity_get_info($this->entityType);
    $entity = $entity_form['#entity'];

    if (!empty($info['fieldable'])) {
      $langcode = entity_language($this->entityType, $entity);
      field_attach_form($this->entityType, $entity, $entity_form, $form_state, $langcode);
    }

    return $entity_form;
  }

}
