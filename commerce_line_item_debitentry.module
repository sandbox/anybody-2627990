<?php

/**
 * @file
 * commerce_line_item_debitentry module.
 */

/**
 * Gets the string to use for the given key.
 */
function commerce_line_item_debitentry_get_string($key) {
  $static = &drupal_static(__FUNCTION__);

  if (!isset($static)) {
    $static = variable_get('commerce_line_item_debitentry_strings', array()) + array(
      'entity label' => t('Commerce line item debit entry entity'),
      'entity plural label' => t('Commerce line item debit entry entities'),
      'entity description' => 'Commerce line item debit entries',
      'type label' => t('Commerce line item debit entry type'),
      'type plural label' => t('Commerce line item debit entry types'),
      'type description' => 'Types of Commerce line item debit entry entities.',
      'base path' => 'admin/commerce/commerce-line-item-debitentries',
      'admin menu path' => 'admin/commerce/config/commerce-line-item-debitentries',
      'admin menu description' => 'Manage Commerce line item debit entry types, including fields.',
      'admin menu path content' => 'admin/commerce/commerce-line-item-debitentries',
    );
  }
  return isset($static[$key]) ? $static[$key] : '';
}

/**
 * Implements hook_entity_info().
 */
function commerce_line_item_debitentry_entity_info() {
  $return = array(
    'commerce_line_item_debitentry' => array(
      'label' => commerce_line_item_debitentry_get_string('entity label'),
      'plural label' => commerce_line_item_debitentry_get_string('entity plural label'),
      'description' => commerce_line_item_debitentry_get_string('entity description'),
      'entity class' => 'CommerceLineItemDebitentryEntity',
      'controller class' => 'CommerceLineItemDebitentryController',
      'base table' => 'commerce_line_item_debitentry_base',
      'revision table' => 'commerce_line_item_debitentry_revision',
      'fieldable' => TRUE,
      // No redirects (from redirect module)
      'redirect' => FALSE,
      'view modes' => array(
        'full' => array(
          'label' => t('Full page'),
          'custom settings' => FALSE,
        ),
      ),
      'entity keys' => array(
        'id' => 'id',
        'revision' => 'revision_id',
        'bundle' => 'type',
        'label' => 'label',
      ),
      'bundles' => array(),
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
      'access callback' => 'commerce_line_item_debitentry_access',
      'module' => 'commerce_line_item_debitentry',
      'metadata controller class' => 'CommerceLineItemDebitentryMetadataController',
      // Enable the entity API's admin UI.
      'admin ui' => array(
        'path' => commerce_line_item_debitentry_get_string('base path'),
        'file' => 'commerce_line_item_debitentry.pages.inc',
        'controller class' => 'CommerceLineItemDebitentryUIController',
      ),
      'path' => commerce_line_item_debitentry_get_string('base path') . '/%entity_object',
      'metatags' => TRUE,
    ),
  );

  // Add bundle info but bypass entity_load() as we cannot use it here.
  $types = commerce_line_item_debitentry_get_bundles();

  foreach ($types as $type_name => $info) {
    $return['commerce_line_item_debitentry']['bundles'][$type_name] = array(
      'label' => $info->label,
      'admin' => array(
        'path' => commerce_line_item_debitentry_get_string('admin menu path') . '/manage/%commerce_line_item_debitentry_type',
        'real path' => commerce_line_item_debitentry_get_string('admin menu path') . '/manage/' . $type_name,
        'bundle argument' => 5,
        'access arguments' => array('administer commerce_line_item_debitentry types'),
      ),
    );
  }

  if (module_exists('uuid')) {
    $return['commerce_line_item_debitentry']['uuid'] = TRUE;
    $return['commerce_line_item_debitentry']['entity keys']['uuid'] = 'uuid';
    $return['commerce_line_item_debitentry']['entity keys']['revision uuid'] = 'vuuid';
  }

  // Support entity cache module.
  if (module_exists('entitycache')) {
    $return['commerce_line_item_debitentry']['field cache'] = FALSE;
    $return['commerce_line_item_debitentry']['entity cache'] = TRUE;
  }

  // Support inline entity form module.
  if (module_exists('inline_entity_form')) {
    $return['commerce_line_item_debitentry']['inline entity form'] = array(
      'controller' => 'CommerceLineItemDebitentryEntityInlineEntityFormController',
    );
  }

  $return['commerce_line_item_debitentry_type'] = array(
    'label' => commerce_line_item_debitentry_get_string('type label'),
    'plural label' => commerce_line_item_debitentry_get_string('type plural label'),
    'description' => commerce_line_item_debitentry_get_string('type description'),
    'entity class' => 'CommerceLineItemDebitentryType',
    'controller class' => 'EntityAPIControllerExportable',
    'base table' => 'commerce_line_item_debitentry_type',
    'fieldable' => FALSE,
    'bundle of' => 'commerce_line_item_debitentry',
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'id',
      'name' => 'name',
      'label' => 'label',
    ),
    'access callback' => 'commerce_line_item_debitentry_type_access',
    'module' => 'commerce_line_item_debitentry',
    // Enable the entity API's admin UI.
    'admin ui' => array(
      'path' => commerce_line_item_debitentry_get_string('admin menu path'),
      'file' => 'commerce_line_item_debitentry.admin.inc',
      'controller class' => 'CommerceLineItemDebitentryTypeUIController',
    ),
  );
  // Define how to get the bundle-name from a commerce_line_item_debitentry type object.
  $return['commerce_line_item_debitentry']['bundle keys']['bundle'] = 'name';

  if (module_exists('uuid')) {
    $return['commerce_line_item_debitentry_type']['uuid'] = TRUE;
    $return['commerce_line_item_debitentry_type']['entity keys']['uuid'] = 'uuid';
  }

  return $return;
}

/**
 * Returns the entity type bundles.
 * @return array
 */
function commerce_line_item_debitentry_get_bundles() {
  $types = db_select('commerce_line_item_debitentry_type', 't')
      ->fields('t')
      ->execute()
      ->fetchAllAssoc('name');

  return $types;
}

/**
 * Access callback for the entity API.
 */
function commerce_line_item_debitentry_access($op, $entity = NULL, $account = NULL) {
  if (user_access('administer commerce_line_item_debitentries', $account)) {
    return TRUE;
  }
  // Allow modules to grant / deny access.
  $access = module_invoke_all('commerce_line_item_debitentry_access', $op, $entity, $account);

  // Only grant access if at least one module granted access and no one denied
  // access.
  if (in_array(FALSE, $access, TRUE)) {
    return FALSE;
  }
  elseif (in_array(TRUE, $access, TRUE)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_commerce_line_item_debitentry_access().
 */
function commerce_line_item_debitentry_commerce_line_item_debitentry_access($op, CommerceLineItemDebitentryEntity $entity = NULL, $account = NULL) {
  $account = isset($account) ? $account : $GLOBALS['user'];

  if ($op == 'view' && user_access('view any commerce_line_item_debitentries', $account)) {
    return TRUE;
  }
  if ($op == 'view' && user_access('view own commerce_line_item_debitentries', $account) && isset($entity) && $entity->uid == $account->uid) {
    return TRUE;
  }
  if ($op == 'create' && user_access('create commerce_line_item_debitentry', $account)) {
    return TRUE;
  }
  if ($op == 'update' && user_access('edit any commerce_line_item_debitentry', $account)) {
    return TRUE;
  }
  if ($op == 'update' && user_access('edit own commerce_line_item_debitentry', $account) && isset($entity) && $entity->uid == $account->uid) {
    return TRUE;
  }
  if ($op == 'delete' && user_access('delete any commerce_line_item_debitentry', $account)) {
    return TRUE;
  }
  if ($op == 'delete' && user_access('delete own commerce_line_item_debitentry', $account) && isset($entity) && $entity->uid == $account->uid) {
    return TRUE;
  }

  // Incorporate per type access permissions.
  if (isset($entity) && $type = $entity->type()) {
    if ($op == 'view' && user_access("view any $type->name commerce_line_item_debitentries", $account)) {
      return TRUE;
    }
    if ($op == 'view' && user_access("view own $type->name commerce_line_item_debitentries", $account) && isset($entity) && $entity->uid == $account->uid) {
      return TRUE;
    }
    if ($op == 'create' && user_access("create $type->name commerce_line_item_debitentry", $account)) {
      return TRUE;
    }
    if ($op == 'update' && user_access("edit any $type->name commerce_line_item_debitentry", $account)) {
      return TRUE;
    }
    if ($op == 'update' && user_access("edit own $type->name commerce_line_item_debitentry", $account) && isset($entity) && $entity->uid == $account->uid) {
      return TRUE;
    }
    if ($op == 'delete' && user_access("delete any $type->name commerce_line_item_debitentry", $account)) {
      return TRUE;
    }
    if ($op == 'delete' && user_access("delete own $type->name commerce_line_item_debitentry", $account) && isset($entity) && $entity->uid == $account->uid) {
      return TRUE;
    }
  }
}

/**
 * Access callback for the entity API.
 */
function commerce_line_item_debitentry_type_access($op, $entity = NULL, $account = NULL) {
  return user_access('administer commerce_line_item_debitentry types', $account);
}

/**
 * Implements hook_permission().
 */
function commerce_line_item_debitentry_permission() {
  $permissions = array(
    'administer commerce_line_item_debitentries' => array(
      'title' => t('Administer @commerce_line_item_debitentries.', array('@commerce_line_item_debitentries' => commerce_line_item_debitentry_get_string('entity plural label'))),
      'description' => t('Created and edit all @commerce_line_item_debitentries.', array('@commerce_line_item_debitentries' => commerce_line_item_debitentry_get_string('entity plural label'))),
    ),
    'administer commerce_line_item_debitentry types' => array(
      'title' => t('Administer @commerce_line_item_debitentry-types.', array('@commerce_line_item_debitentry-types' => commerce_line_item_debitentry_get_string('type plural label'))),
      'description' => t('Create and delete @commerce_line_item_debitentry-types and their fields.', array('@commerce_line_item_debitentry-types' => commerce_line_item_debitentry_get_string('type plural label'))),
    ),
    'view own commerce_line_item_debitentries' => array(
      'title' => t('View own @commerce_line_item_debitentries.', array('@commerce_line_item_debitentries' => commerce_line_item_debitentry_get_string('entity plural label'))),
    ),
    'view any commerce_line_item_debitentries' => array(
      'title' => t('View any @commerce_line_item_debitentries.', array('@commerce_line_item_debitentries' => commerce_line_item_debitentry_get_string('entity plural label'))),
    ),
    "create commerce_line_item_debitentry" => array(
      'title' => t('Create new @commerce_line_item_debitentry', array('@commerce_line_item_debitentry' => commerce_line_item_debitentry_get_string('entity label'))),
    ),
    "edit own commerce_line_item_debitentry" => array(
      'title' => t('Edit own @commerce_line_item_debitentry', array('@commerce_line_item_debitentry' => commerce_line_item_debitentry_get_string('entity label'))),
    ),
    "edit any commerce_line_item_debitentry" => array(
      'title' => t('Edit any @commerce_line_item_debitentry', array('@commerce_line_item_debitentry' => commerce_line_item_debitentry_get_string('entity label'))),
    ),
    "delete own commerce_line_item_debitentry" => array(
      'title' => t('Delete own @commerce_line_item_debitentry', array('@commerce_line_item_debitentry' => commerce_line_item_debitentry_get_string('entity label'))),
    ),
    "delete any commerce_line_item_debitentry" => array(
      'title' => t('Delete any @commerce_line_item_debitentry', array('@commerce_line_item_debitentry' => commerce_line_item_debitentry_get_string('entity label'))),
    ),
  );
  foreach (entity_load('commerce_line_item_debitentry_type') as $type) {
    $permissions += _commerce_line_item_debitentry_list_permissions($type);
  }
  return $permissions;
}

/**
 * Helper function to generate standard commerce_line_item_debitentry permission list for a given type.
 *
 * @param commerce_line_item_debitentryType $type
 *   The machine-readable name of the node type.
 *
 * @return array
 *   An array of permission names and descriptions.
 */
function _commerce_line_item_debitentry_list_permissions(CommerceLineItemDebitentryType $type) {
  // Build standard list of permissions for this type.
  return array(
    "view own $type->name commerce_line_item_debitentries" => array(
      'title' => t('%type_label: View own @commerce_line_item_debitentries.', array('%type_label' => $type->label, '@commerce_line_item_debitentries' => commerce_line_item_debitentry_get_string('entity plural label'))),
    ),
    "view any $type->name commerce_line_item_debitentries" => array(
      'title' => t('%type_label: View any @commerce_line_item_debitentries.', array('%type_label' => $type->label, '@commerce_line_item_debitentries' => commerce_line_item_debitentry_get_string('entity plural label'))),
    ),
    "create $type->name commerce_line_item_debitentry" => array(
      'title' => t('%type_label: Create new @commerce_line_item_debitentry', array('%type_label' => $type->label, '@commerce_line_item_debitentry' => commerce_line_item_debitentry_get_string('entity label'))),
    ),
    "edit own $type->name commerce_line_item_debitentry" => array(
      'title' => t('%type_label: Edit own @commerce_line_item_debitentry', array('%type_label' => $type->label, '@commerce_line_item_debitentry' => commerce_line_item_debitentry_get_string('entity label'))),
    ),
    "edit any $type->name commerce_line_item_debitentry" => array(
      'title' => t('%type_label: Edit any @commerce_line_item_debitentry', array('%type_label' => $type->label, '@commerce_line_item_debitentry' => commerce_line_item_debitentry_get_string('entity label'))),
    ),
    "delete own $type->name commerce_line_item_debitentry" => array(
      'title' => t('%type_label: Delete own @commerce_line_item_debitentry', array('%type_label' => $type->label, '@commerce_line_item_debitentry' => commerce_line_item_debitentry_get_string('entity label'))),
    ),
    "delete any $type->name commerce_line_item_debitentry" => array(
      'title' => t('%type_label: Delete any @commerce_line_item_debitentry', array('%type_label' => $type->label, '@commerce_line_item_debitentry' => commerce_line_item_debitentry_get_string('entity label'))),
    ),
  );
}

/**
 * commerce_line_item_debitentry type loader.
 *
 * @return commerce_line_item_debitentryType
 */
function commerce_line_item_debitentry_type_load($type_name) {
  return entity_load_single('commerce_line_item_debitentry_type', $type_name);
}

/**
 * commerce_line_item_debitentry loader.
 *
 * @return commerce_line_item_debitentry
 */
function commerce_line_item_debitentry_load($id) {
  return entity_load_single('commerce_line_item_debitentry', $id);
}

/**
 * Implements hook_views_api().
 */
function commerce_line_item_debitentry_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_ctools_plugin_directory()
 */
function commerce_line_item_debitentry_ctools_plugin_directory($module, $plugin) {
  if (in_array($module, array('panelizer', 'ctools', 'page_manager'))) {
    return 'plugins/' . $plugin;
  }
}

/**
 * Implements hook_ctools_plugin_api().
 */
function commerce_line_item_debitentry_ctools_plugin_api($module, $api) {
  if (($module == 'page_manager' && $api == 'pages_default') || $module == 'panelizer') {
    return array(
      'version' => 1,
      'path' => drupal_get_path('module', 'panelizer') . '/includes',
    );
  }
}

/**
 * Implements hook_menu_local_tasks_alter().
 */
function commerce_line_item_debitentry_menu_local_tasks_alter(&$data, $router_item, $root_path) {
  // Add action link to add commerce_line_item_debitentries on the admin menu content page.
  if ($root_path == commerce_line_item_debitentry_get_string('admin menu path content')) {
    $item = menu_get_item(commerce_line_item_debitentry_get_string('base path') . '/add');
    if ($item['access']) {
      $data['actions']['output'][] = array(
        '#theme' => 'menu_local_action',
        '#link' => $item,
      );
    }
  }
}

/**
 * Implements HOOK_enable().
 */
function commerce_line_item_debitentry_enable() {
  // TODO - webksde#JP Add bundle coupon_debit programatically.
  // TODO - webksde#JP Add fields programatically. See http://drupal.stackexchange.com/questions/8284/programmatically-create-fields-in-drupal-7
}

/**
 * Returns an array of all debit entries of the given commerce_line_item.
 *
 * @param stdClass $commerce_line_item
 * @return array
 */
function commerce_line_item_debitentry_get_debitentries_of_line_item(stdClass $commerce_line_item) {
  if (!empty($commerce_line_item) && !empty($commerce_line_item->line_item_id)) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'commerce_line_item_debitentry')
        ->fieldCondition('field_coupon_debit_line_item', 'target_id', $commerce_line_item->line_item_id);
    $result = $query->execute();

    $debitentries = array();
    if (!empty($result['commerce_line_item_debitentry'])) {
      foreach ($result['commerce_line_item_debitentry'] as $id => $debitentry_array) {
        $debitentries[] = entity_load_single('commerce_line_item_debitentry', $debitentry_array->id);
      }
      return $debitentries;
    }
    else {
      return array();
    }
  }
  else {
    return array();
  }
}

/**
 * Returns the price amount of the given $commerce_line_item_debitentry.
 *
 * @param stdClass $commerce_line_item_debitentry
 * @return decimal
 */
function commerce_line_item_debitentry_get_debitentry_total(CommerceLineItemDebitentryEntity $commerce_line_item_debitentry) {
  $commerce_line_item_debitentry_wrapper = entity_metadata_wrapper('commerce_line_item_debitentry', $commerce_line_item_debitentry);
  $commerce_line_item_debitentry_total = 0;
  if (isset($commerce_line_item_debitentry_wrapper->commerce_price)) {
    $commerce_line_item_debitentry_total_array = $commerce_line_item_debitentry_wrapper->commerce_price->value();
    if (is_array($commerce_line_item_debitentry_total_array)) {
      $commerce_line_item_debitentry_total = $commerce_line_item_debitentry_total_array['amount'];
    }
  }
  return $commerce_line_item_debitentry_total;
}

function commerce_line_item_debitentry_get_line_item_debitentries_sum(stdClass $commerce_line_item) {
  $commerce_line_item_debitentries = commerce_line_item_debitentry_get_debitentries_of_line_item($commerce_line_item);
  return commerce_line_item_debitentry_get_debitentries_sum($commerce_line_item_debitentries);
}

function commerce_line_item_debitentry_get_line_item_debitentries_sum_formatted(stdClass $commerce_line_item) {
  $amount = commerce_line_item_debitentry_get_line_item_debitentries_sum($commerce_line_item);

  return commerce_currency_format($amount, 'EUR');
}

function commerce_line_item_debitentry_get_debitentries_sum(array $commerce_line_item_debitentries) {
  $sum = 0;
  if (!empty($commerce_line_item_debitentries)) {
    foreach ($commerce_line_item_debitentries as $commerce_line_item_debitentry) {
      $sum += commerce_line_item_debitentry_get_debitentry_total($commerce_line_item_debitentry);
    }
  }
  return $sum;
}

/**
 * Returns the balance (rest amount) of the given commerce_line_item by calculating
 * {commerce_line_item->Price MINUS debitentries_sum} of all related debitentries of the lineitem.
 *
 * @param stdClass $commerce_line_item
 * @return int
 */
function commerce_line_item_debitentry_get_line_item_balance(stdClass $commerce_line_item) {
  $line_item_total = commerce_line_item_debitentry_get_line_item_total($commerce_line_item);
  $debitentries_sum = commerce_line_item_debitentry_get_line_item_debitentries_sum($commerce_line_item);
  $balance = $line_item_total - $debitentries_sum;
  return $balance;
}

function commerce_line_item_debitentry_get_line_item_balance_formatted(stdClass $commerce_line_item) {
  $amount = commerce_line_item_debitentry_get_line_item_balance($commerce_line_item);
  return commerce_currency_format($amount, 'EUR');
}

function commerce_line_item_debitentry_get_line_item_total(stdClass $commerce_line_item) {
  $line_item_total = 0;
  $commerce_line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $commerce_line_item);
  if (is_object($commerce_line_item_wrapper) && isset($commerce_line_item_wrapper->commerce_total)) {
    $line_item_total_array = $commerce_line_item_wrapper->commerce_total->value();
    if (is_array($line_item_total_array)) {
      $line_item_total = $line_item_total_array['amount'];
    }
  }
  return $line_item_total;
}

/**
 * Returns TRUE if it is allowed to add the given debit entry amount as debit for the line item.
 * Checks for the consistency condition that the balance may never be lower than 0.
 * Returns FALSE if the balance would become < 0.
 *
 * @param stdClass $commerce_line_item
 * @param decimal $debit_amount
 */
function commerce_line_item_debitentry_is_debit_amount_allowed(stdClass $commerce_line_item, $debitentry_amount) {
  return commerce_line_item_debitentry_get_line_item_balance($commerce_line_item) - $debitentry_amount >= 0;
}

/**
 * Returns the debit status of the given line_item.
 * Open = balance > 0
 * Fully redeemed = Balance = 0
 *
 * @param stdClass $commerce_line_item
 * @return type
 */
function commerce_line_item_debitentry_get_line_item_debit_status(stdClass $commerce_line_item) {
  return commerce_line_item_debitentry_get_line_item_balance($commerce_line_item) > 0 ? t('Open') : t('Fully redeemed');
}

/**
 * TODO Documentation
 * 
 * @param stdClass $commerce_line_item
 * @return boolean
 */
function commerce_line_item_debitentry_get_line_item_debit_expiration_timestamp(stdClass $commerce_line_item) {
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $commerce_line_item);
  if (!isset($commerce_line_item->order_id) || empty($commerce_line_item->order_id)) {
    return FALSE;
  }
  $commerce_order_id = $line_item_wrapper->order_id->value();
  $commerce_order_wrapper = entity_metadata_wrapper('commerce_order', $commerce_order_id);

  $order_date_timestamp = $commerce_order_wrapper->field_commerce_billy_i_date->value();

  // TODO - Variable
  // 3 Years
  $expiration_date_timestamp = $order_date_timestamp + (3600 * 24 * 365 * 3);

  return $expiration_date_timestamp;
}

/**
 * TODO Documentation
 *
 * @param stdClass $commerce_line_item
 * @return type
 */
function commerce_line_item_debitentry_get_line_item_debit_expiration_formatted(stdClass $commerce_line_item) {
  return format_date(commerce_line_item_debitentry_get_line_item_debit_expiration_timestamp($commerce_line_item, 'short'));
}

/**
 * Implements hook_field_extra_fields().
 */
function commerce_line_item_debitentry_field_extra_fields() {
  $extra = array();
  // Add extra fields to debitentries
  $debitentry_bundles = commerce_line_item_debitentry_get_bundles();
  if (!empty($debitentry_bundles)) {
//    foreach ($debitentry_bundles as $bundle_name => $bundle_info) {
//      $extra['commerce_line_item_debitentry'][$bundle_name]['display']['welcome_message'] = array(
//        'label' => t('Welcome message'),
//        'description' => t('A welcome message'),
//        'weight' => 0,
//      );
//    }
  }

  // Add extra fields to lineitems
  $line_item_bundles = _commerce_line_item_debitentry_get_line_item_bundles();
  if (!empty($line_item_bundles)) {
    foreach ($line_item_bundles as $bundle_name => $bundle_info) {
      $extra['commerce_line_item'][$bundle_name]['display']['debitentries_balance'] = array(
        'label' => t('Debit balance'),
        'description' => t('The balance of the line item price minus all its debits.'),
        'weight' => 0,
        // Callback function required by extrafield_views_integration.module
        'callback' => 'commerce_line_item_debitentry_get_line_item_balance_formatted',
      );

      $extra['commerce_line_item'][$bundle_name]['display']['debitentries_sum'] = array(
        'label' => t('Debit sum'),
        'description' => t('The sum of all debits of this line item.'),
        'weight' => 0,
        // Callback function required by extrafield_views_integration.module
        'callback' => 'commerce_line_item_debitentry_get_line_item_debitentries_sum_formatted',
      );

      $extra['commerce_line_item'][$bundle_name]['display']['debit_status'] = array(
        'label' => t('Debit status'),
        'description' => t('Deprecated! Use the computed field instead which is more flexibel. The debit status of the line item. %booked_up (if balance = 0) or %open (if balance > 0)', array('%booked_up' => t('Fully redeemed'), '%open' => t('Open'))),
        'weight' => 0,
        // Callback function required by extrafield_views_integration.module
        'callback' => 'commerce_line_item_debitentry_get_line_item_debit_status',
      );

      $extra['commerce_line_item'][$bundle_name]['display']['debit_expiration'] = array(
        'label' => t('Expiration date'),
        'description' => t('The expiration date of the line item coupon.'),
        'weight' => 0,
        // Callback function required by extrafield_views_integration.module
        'callback' => 'commerce_line_item_debitentry_get_line_item_debit_expiration_formatted',
      );
    }
  }

  // Add extra fields to debitentries:
  $extra['commerce_line_item_debitentry']['coupon_debit']['display']['full_product_title'] = array(
    'label' => t('Full product title'),
    'description' => t('The full title of the related product.'),
    'weight' => 0,
    // Callback function required by extrafield_views_integration.module
    'callback' => 'commerce_line_item_debitentry_get_full_product_title',
  );

  return $extra;
}

/**
 * Callback function for commerce_line_item_debitentry_field_extra_fields().
 */
function commerce_line_item_debitentry_get_full_product_title(CommerceLineItemDebitentryEntity $commerce_line_item_debitentry) {
  $commerce_line_item_debitentry_wrapper = entity_metadata_wrapper('commerce_line_item_debitentry', $commerce_line_item_debitentry);
  $commerce_product = $commerce_line_item_debitentry_wrapper->field_coupon_debit_product->value();
  return _drowl_customer_commerce_product_get_full_title($commerce_product);
}

/**
 * Helper function to return an array of all line item bundle types existing.
 * @return type
 */
function _commerce_line_item_debitentry_get_line_item_bundles() {
  $entity_info = entity_get_info('commerce_line_item');
  if (!empty($entity_info['bundles'])) {
    return $entity_info['bundles'];
  }
  else {
    return array();
  }
}

/**
 * Helper function to return the VAT rate (0.07 / 0.19 / ...) by the debitentrys product.
 *
 * @param CommerceLineItemDebitentryEntity $commerce_line_item_debitentry
 * @return decimal
 */
function commerce_line_item_debitentry_calculate_vat_rate(CommerceLineItemDebitentryEntity $commerce_line_item_debitentry) {
  $commerce_line_item_debitentry_wrapper = entity_metadata_wrapper('commerce_line_item_debitentry', $commerce_line_item_debitentry);
  $commerce_product = $commerce_line_item_debitentry_wrapper->field_coupon_debit_product->value();
  $commerce_line_item = $commerce_line_item_debitentry_wrapper->field_coupon_debit_line_item->value();
  $commerce_line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $commerce_line_item);

  $commerce_product_wrapper = entity_metadata_wrapper('commerce_product', $commerce_product);
  // Check if the product has a VAT set.
  if (!empty($commerce_product->commerce_de_vat)) {
    $commerce_de_vat_rate = $commerce_product_wrapper->commerce_de_vat->value();
    $vat_rate = commerce_vat_rate_load($commerce_de_vat_rate);

    // From commerce_vat
    if ($commerce_line_item_wrapper->order->created->value()) {
      $order_date = $commerce_line_item_wrapper->order->created->value();
    }
    else {
      $order_date = time();
    }
    foreach ($vat_rate['rates'] as $rate) {
      if (strtotime($rate['start']) < $order_date) {
        $rate_info = $rate;
        break;
      }
    }
    // From commerce_vat END

    $rate_decimal = FALSE;
    if (is_array($rate_info) && isset($rate_info['rate'])) {
      // 0.07 for example:
      $rate_decimal = $rate_info['rate'];
    }
  }
  else {
    // No VAT set. VAT rate = 0.
    $rate_decimal = 0;
  }

  return $rate_decimal;
}

function commerce_line_item_debitentry_calculate_vat_amount(CommerceLineItemDebitentryEntity $commerce_line_item_debitentry) {
  $vat_rate = commerce_line_item_debitentry_calculate_vat_rate($commerce_line_item_debitentry);
  $debitentry_total = commerce_line_item_debitentry_get_debitentry_total($commerce_line_item_debitentry);

  $vat_amount = commerce_vat_rate_round_amount($debitentry_total - ($debitentry_total / (1 + $vat_rate)));
  return $vat_amount;
}