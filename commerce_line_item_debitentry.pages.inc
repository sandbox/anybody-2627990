<?php

/**
 * @file
 * White label entity editing UI.
 */

/**
 * commerce_line_item_debitentry UI controller.
 */
class CommerceLineItemDebitentryUIController extends EntityBundleableUIController {

  /**
   * Provides definitions for implementing hook_menu().
   */
  public function hook_menu() {
    $items = parent::hook_menu();

    // Extend the 'add' path.
    $items[$this->path . '/add']['title'] = 'Add ' . commerce_line_item_debitentry_get_string('entity label');
    return $items;
  }

  /**
   * Operation form submit callback.
   */
  public function operationFormSubmit($form, &$form_state) {
    parent::operationFormSubmit($form, $form_state);
    $form_state['redirect'] = commerce_line_item_debitentry_get_string('admin menu path content');
  }

}

/**
 * Generates the commerce_line_item_debitentry editing form.
 */
function commerce_line_item_debitentry_form($form, &$form_state, Entity $commerce_line_item_debitentry, $op = 'edit') {

  // Needed by entity_form_field_validate().
  $form['type'] = array('#type' => 'value', '#value' => $commerce_line_item_debitentry->type);

//  $form['label'] = array(
//    '#type' => 'textfield',
//    '#title' => t('Label'),
//    '#default_value' => isset($commerce_line_item_debitentry->label) ? $commerce_line_item_debitentry->label : '',
//    '#description' => t('The label associated with the @commerce_line_item_debitentry.', array('@commerce_line_item_debitentry' => commerce_line_item_debitentry_get_string('entity label'))),
//    '#required' => TRUE,
//    '#weight' => -15,
//  );
  // Technical creation:
  $form['created'] = array(
    '#type' => 'textfield',
    '#title' => t('Creation date'),
    '#default_value' => isset($commerce_line_item_debitentry->created) ? format_date($commerce_line_item_debitentry->created, 'short') : '',
    '#description' => t('The date when the @commerce_line_item_debitentry was created.', array('@commerce_line_item_debitentry' => commerce_line_item_debitentry_get_string('entity label'))),
    '#required' => TRUE,
    '#weight' => 99,
    // Hide for everyone!
    '#access' => FALSE,
  );

  $form['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate this @commerce_line_item_debitentry', array('@commerce_line_item_debitentry' => commerce_line_item_debitentry_get_string('entity label'))),
    '#default_value' => !empty($commerce_line_item_debitentry->status),
    '#weight' => 100,
    // Hide by default:
    '#access' => FALSE,
  );

  $form['options']['#tree'] = TRUE;
  $form['options']['#weight'] = 101;
  if ($commerce_line_item_debitentry->type()->data['supports_revisions'] && empty($commerce_line_item_debitentry->is_new)) {
    $form['options']['create_revision'] = array(
      '#type' => 'checkbox',
      '#title' => t('Create a new revision'),
      '#default_value' => TRUE,
      '#description' => t('Create a new revision for this edit.'),
      '#weight' => 101,
    );
  }

  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 200
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 200,
  );

  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#weight' => 201,
    '#limit_validation_errors' => array(),
    '#submit' => array('commerce_line_item_debitentry_form_submit_delete'),
    '#access' => ($op != 'add' && $op != 'clone') && commerce_line_item_debitentry_access('delete', $commerce_line_item_debitentry)
  );

  field_attach_form('commerce_line_item_debitentry', $commerce_line_item_debitentry, $form, $form_state);

  return $form;
}

/**
 * commerce_line_item_debitentry editing form validation callback.
 */
function commerce_line_item_debitentry_form_validate(&$form, &$form_state) {
  entity_form_field_validate('commerce_line_item_debitentry', $form, $form_state);
  $values = $form_state['values'];
  if (!empty($values['field_coupon_debit_line_item'][LANGUAGE_NONE][0]['target_id'])) {
    _commerce_line_item_debitentry_balance_validate($form, $form_state);
  }
  else {
    form_error($form, t('No line item reference given. Debit entries require a line item reference.'));
  }
}

/**
 * Helper function to ensure the debit balance is valid.
 */
function _commerce_line_item_debitentry_balance_validate(&$form, &$form_state) {
  $values = $form_state['values'];
  $commerce_line_item_id = $values['field_coupon_debit_line_item'][LANGUAGE_NONE][0]['target_id'];
  $commerce_line_item = commerce_line_item_load($commerce_line_item_id);
  $debitentry_price = $values['commerce_price'][LANGUAGE_NONE][0];
  $debitentry_price_amount = $debitentry_price['amount'];
  $debitentry_price_currency = $debitentry_price['currency_code'];
  if ($form_state['op'] != 'add') {
    // If the debitentry already exists and is being changed only, we have to substract the current value from the form, otherwise it would be
    // checked for double the value.
    $debitentry_price_current = $form['#entity']->commerce_price[LANGUAGE_NONE][0];
    $debitentry_price_amount_current = $debitentry_price_current['amount'];
    $debitentry_price_amount = $debitentry_price_amount - $debitentry_price_amount_current;
  }
  if (!commerce_line_item_debitentry_is_debit_amount_allowed($commerce_line_item, $debitentry_price_amount)) {
    $commerce_line_item_total = commerce_line_item_debitentry_get_line_item_total($commerce_line_item);
    form_set_error('commerce_price', t('The entered debit amounts sum (@debit_amount + others) may not be higher than the associated line item total amount (@commerce_line_item_total).', array('@debit_amount' => commerce_currency_format($debitentry_price_amount, $debitentry_price_currency), '@commerce_line_item_total' => commerce_currency_format($commerce_line_item_total, $debitentry_price_currency))));
  }
}

/**
 * Form API submit callback for the commerce_line_item_debitentry form.
 */
function commerce_line_item_debitentry_form_submit(&$form, &$form_state) {
  $date_format = variable_get('date_format_short', 'm/d/Y - H:i');
  $date = date_parse_from_format($date_format, $form_state['values']['created']);
  $form_state['values']['created'] = mktime($date['hour'], $date['minute'], $date['second'], $date['month'], $date['day'], $date['year']);
  $commerce_line_item_debitentry = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  $commerce_line_item_debitentry->save();
  $info = $commerce_line_item_debitentry->entityInfo();
  if (is_subclass_of($info['admin ui']['controller class'], 'EntityContentUIController')) {
    $form_state['redirect'] = commerce_line_item_debitentry_get_string('base path') . '/' . $commerce_line_item_debitentry->id;
  }
  else {
    $form_state['redirect'] = commerce_line_item_debitentry_get_string('admin menu path content');
  }
}

/**
 * Form API submit callback for the delete button.
 */
function commerce_line_item_debitentry_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = commerce_line_item_debitentry_get_string('base path') . '/' . $form_state['commerce_line_item_debitentry']->id . '/delete';
}